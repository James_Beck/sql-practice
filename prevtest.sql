USE master
GO

DROP DATABASE test1
GO

CREATE DATABASE test1
GO

USE test1
GO

CREATE TABLE Customers (
CustomerID int PRIMARY KEY,
FName varchar(20) NOT NULL,
LName varchar(20) NOT NULL,
Sport varchar(20) NOT NULL,
)

CREATE TABLE Products (
ProductID int PRIMARY KEY,
ProductDescription VARCHAR(20),
UnitPrice REAL,
Category VARCHAR(20))

CREATE TABLE Purchases(
OrderID int IDENTITY(1,1) PRIMARY KEY,
CustomerID int NOT NULL, 
ProductID int NOT NULL,
Quantity int,
PurchaseDate DATE NOT NULL,
FOREIGN KEY (CustomerID) REFERENCES Customers (CustomerID),
FOREIGN KEY (ProductID) REFERENCES Products (ProductID))

insert into Customers values (1,'Cristiano','Ronaldo','soccer')
insert into Customers values (2,'Daniel','Carter','rugby')
insert into Customers values (3,'LeBron','James','basketball')
insert into Customers values (4,'Lydia','Ko','golf')
insert into Customers values (5,'Nilesh','Kanji','squash')
insert into Customers values (6,'Roger','Federer','tennis')
insert into Customers values (7,'Ronda','Rousey','fighting')
insert into Customers values (8,'Tiger','Woods','golf')
insert into Customers values (9,'Usain','Bolt','sprinting')
insert into Customers values (10,'Valarie','Adams','shot put')


insert into Products values (1, 'Bag', 42, 'equipment')
insert into Products values (2, 'Cap', 29, 'headwear')
insert into Products values (3, 'Shirt', 35, 'clothing')
insert into Products values (4, 'Shorts', 22, 'clothing')
insert into Products values (5, 'Socks', 8, 'clothing')
insert into Products values (6, 'Sun Block', 5, 'equipment')
insert into Products values (7, 'Sun Glasses', 78, 'headwear')
insert into Products values (8, 'Wax', 14, 'equipment')


insert into Purchases values (1,2,4,'2016-4-6')
insert into Purchases values (2,3,1,'2016-4-7')
insert into Purchases values (4,4,2,'2016-4-7')
insert into Purchases values (4,5,1,'2016-4-7')
insert into Purchases values (3,6,2,'2016-4-7')
insert into Purchases values (4,4,3,'2016-4-8')
insert into Purchases values (4,7,1,'2016-4-8')
insert into Purchases values (2,1,1,'2016-4-8')
insert into Purchases values (5,5,6,'2016-4-8')
insert into Purchases values (3,3,1,'2016-4-8')
insert into Purchases values (1,5,2,'2016-4-9')
insert into Purchases values (2,3,1,'2016-4-9')
insert into Purchases values (8,2,2,'2016-4-9')
insert into Purchases values (5,4,7,'2016-4-9')
insert into Purchases values (3,7,1,'2016-4-9')
insert into Purchases values (1,1,2,'2016-4-10')
insert into Purchases values (9,4,1,'2016-4-10')
insert into Purchases values (9,2,2,'2016-4-10')
insert into Purchases values (6,5,1,'2016-4-10')
insert into Purchases values (5,4,4,'2016-4-10')
insert into Purchases values (1,5,2,'2016-4-13')
insert into Purchases values (4,7,1,'2016-4-13')
insert into Purchases values (5,4,5,'2016-4-13')
insert into Purchases values (3,3,2,'2016-4-13')
insert into Purchases values (9,2,2,'2016-4-14')
insert into Purchases values (2,4,1,'2016-4-14')
insert into Purchases values (5,5,8,'2016-4-14')
insert into Purchases values (9,3,3,'2016-4-15')
insert into Purchases values (3,5,4,'2016-4-15')
insert into Purchases values (1,6,3,'2016-5-02')
insert into Purchases values (3,2,1,'2016-5-03')
insert into Purchases values (1,2,5,'2016-5-04')
