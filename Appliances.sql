USE master
GO

DROP DATABASE test1
GO

CREATE DATABASE test1
GO

USE test1
GO

CREATE TABLE Appliances (
ID char(3) PRIMARY KEY,
AppType varchar(20), 
StoreID char(2),
Cost real,
Price real)

CREATE TABLE Stores (
StoreID char(2) PRIMARY KEY,
City varchar(30) NOT NULL,
Rating int)

CREATE TABLE Salespeople (
EmployeeID char(4) PRIMARY KEY,
EmployeeName varchar(30) NOT NULL,
CommRate int,
BaseSalary int,
SupervisorID char(4))

CREATE TABLE Sales (
SaleDate date,
EmployeeID char(4),
AppID char(3),
Qty int)

insert into Salespeople values ('1235', 'Linda Smith', 15, 1200, '1412')
insert into Salespeople values ('1412', 'Anne Green', 12, 1800, NULL)
insert into Salespeople values ('2920', 'Charles Brown', 10, 1150, '1412')
insert into Salespeople values ('3231', 'Harry Purple', 18, 1700, '1412')

insert into Stores values ('22', 'Hamilton', 8)
insert into Stores values ('20', 'Te Awamutu', 6)
insert into Stores values ('27', 'Huntly', 9)

insert into Appliances values ('100', 'Refrigerator', '22', 150, 250)
insert into Appliances values ('150', 'Television', '27', 225, 340)
insert into Appliances values ('110', 'Refrigerator', '20', 175, 300)
insert into Appliances values ('200', 'Microwave Oven', '22', 120, 180)
insert into Appliances values ('300', 'Washer', '27', 200, 325)
insert into Appliances values ('310', 'Washer', '22', 280, 400)
insert into Appliances values ('400', 'Dryer', '20', 150, 220)
insert into Appliances values ('420', 'Dryer', '22', 240, 360)

insert into Sales values ('2010/1/1','1412','150',1)
insert into Sales values ('2010/1/5','3231','110',1)
insert into Sales values ('2010/1/3','2920','110',2)
insert into Sales values ('2010/1/13','1412','100',1)
insert into Sales values ('2010/1/25','1235','150',2)
insert into Sales values ('2010/1/22','1235','100',2)
insert into Sales values ('2010/1/12','2920','150',3)
insert into Sales values ('2010/1/14','3231','100',1)
insert into Sales values ('2010/1/15','1235','300',1)
insert into Sales values ('2010/1/3','2920','200',2)
insert into Sales values ('2010/1/31','2920','310',1)
insert into Sales values ('2010/1/5','1412','420',1)
insert into Sales values ('2010/1/15','3231','400',2)

--1. Display all salespeople with a base salary between $1000 and $1500, inclusive.
--OUTPUT: EmployeeName BaseSalary
		--Linda Smith    1200
		--Charles Brown  1150
SELECT EmployeeName, BaseSalary
FROM Salespeople
WHERE BaseSalary BETWEEN 1000 AND 1500
-- Completed

--2. Use the set construction operator IN to display the appliances available from stores 20 and 27.
--OUTPUT: ID	AppType		StoreID
		  --   ---------   ---------
		--110 Refrigerator   20
		--150 Television     27
		--300 Washer         27
		--400 Dryer          20
SELECT ID, AppType, StoreID
FROM Appliances
WHERE StoreID IN
(
	SELECT StoreID
	FROM Appliances
	WHERE StoreID LIKE '20' OR StoreID LIKE '27'
)
-- Completed
-- Note, the IN operator is a test to see if something you're looking for is within a list
--		The list, often generated with by a query of it's own

--3. List the employee names who have no supervisor.
--OUTPUT: EmployeeName
         --------------
	   -- Anne Green
SELECT EmployeeName
FROM Salespeople
WHERE SupervisorID IS NULL
-- Completed

--4. What is the total base salary being paid each month?
--OUTPUT: Total Base Salary
 		  -----------------
			--5850
SELECT SUM(BaseSalary) AS [Total Base Salary]
FROM SalesPeople
-- Completed
--Notes: When in square brackets, names can hold white space

--5. List total number of appliance sales by appliance ID where there were more than two sales for that ID in the month.
--OUTPUT: AppID Total sales
		  ----- -----------
		 --100       4
		 --110       3
		 --150       6
SELECT AppID, SUM(Qty) AS [Total Sales]
FROM Sales
GROUP BY AppID
HAVING SUM(Qty) > 2
-- Completed

--6. List the appliance IDs of all washers and all appliances sold by Linda Smith.
--OUTPUT: AppID
		  -----
		 --100
		 --150
		 --300
		 --310
SELECT AppID
FROM Sales S, Salespeople P
WHERE S.EmployeeID = P.EmployeeID
AND P.EmployeeName LIKE 'Linda Smith'
-- Completed
-- Notes: The aliases for a table need to follow the name of the table

--7. For each appliance type, list the average price of the appliance.
--OUTPUT: AppType	Average Price
		  -------   -------------
		--Dryer          290
		--Microwave Oven 180
		--Refrigerator   275
		--Television     340
		--Washer         362.5
SELECT AppType, AVG(Price)
FROM Appliances
GROUP BY AppType
-- Completed

--8. List the Appliances types and the number of Appliancess of each type.
--OUTPUT: AppType   Number of Appliances
		  -------   --------------------
		--Dryer               2
		--Microwave Oven      1
		--Refrigerator        2
		--Television          1
		--Washer              2
SELECT AppType, COUNT(AppType)
FROM Appliances
GROUP BY AppType 
-- Completed

--9. List the IDs of appliances sold by more than one salesperson.
--OUTPUT: AppID
		  -----
		--100
		--110
		--150
SELECT AppID
FROM Sales
GROUP BY AppID
HAVING COUNT(AppID) > 1
-- Completed


